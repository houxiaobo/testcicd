import static org.junit.jupiter.api.Assertions.*;

class HelloWorldTest {
    HelloWorld helloWorld;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        helloWorld = new HelloWorld();
    }

    @org.junit.jupiter.api.Test
    void sayHello() {
        assertEquals("Hello World", helloWorld.sayHello());
    }
}